<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page</title>
</head>
<body>
		<h1>Welcome <%=session.getAttribute("fullName") %>! </h1>
		
		<%
			String workerType = session.getAttribute("workerType").toString();
			
			//Condition for what message will prompted
			if(workerType.equals("applicant")){
				out.println("Welcome applicant. You may now start looking for your career opportunity.");
				
			}else{
				out.println("Welcome employer. You may start browsing applicant profiles.");
			}
		%>

	<!-- <p>You may now start browsing applicant profiles</p> -->

</body>
</html>